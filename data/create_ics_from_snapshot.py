
import h5py
import sys

def ics_from_snapshot(inputfile, outputfile):
    f_input  = h5py.File(inputfile, 'r')
    f_output = h5py.File(outputfile, 'w')

    f_input.copy('Header', f_output)
    f_input.copy('Units', f_output)

    g_PT0 = f_output.create_group('PartType0')
    g_PT4 = f_output.create_group('PartType4')

    f_input.copy('PartType0/Coordinates', g_PT0)
    f_input.copy('PartType0/InternalEnergies', g_PT0)  # InternalEnergy
    f_input.copy('PartType0/Masses', g_PT0)
    f_input.copy('PartType0/ParticleIDs', g_PT0)
    f_input.copy('PartType0/SmoothingLengths', g_PT0)   # SmoothingLength
    f_input.copy('PartType0/Velocities', g_PT0)

    f_input.copy('PartType4/Coordinates', g_PT4)
    f_input.copy('PartType4/Masses', g_PT4)
    f_input.copy('PartType4/ParticleIDs', g_PT4)
    f_input.copy('PartType4/BirthTimes', g_PT4)  # StellarFormationTime 
    f_input.copy('PartType4/Velocities', g_PT4)

    f_output['PartType4/StellarFormationTime'] = f_output['PartType4/BirthTimes']
    del f_output['PartType4/BirthTimes']

    f_output['PartType0/SmoothingLength'] = f_output['PartType0/SmoothingLengths']
    del f_output['PartType0/SmoothingLengths']

    f_output['PartType0/InternalEnergy'] = f_output['PartType0/InternalEnergies']
    del f_output['PartType0/InternalEnergies']

    grp = f_output.create_group('Info')
    grp.attrs["Input file"] = inputfile
     
    f_input.close()
    f_output.close()

    return
