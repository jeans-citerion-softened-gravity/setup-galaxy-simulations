import os
from pathlib import Path
import shutil
import glob
import re

from data.create_ics_from_snapshot import ics_from_snapshot

##########################################
# Select mass resolution and snapshotnumber
##########################################
mass_resolution_levels = ["M6"]
snapshotnumber = 85
##########################################



localdir = os.getcwd()
for mass in mass_resolution_levels:
    os.chdir(os.path.join(localdir,mass))
    for runfolder in glob.glob("Galaxy*/"):
        ##########################################
        # setting runfolder name and creating rerunfolder
        ##########################################
        runfolder_absolute_path   = os.path.join(localdir, mass, runfolder)
        rerunfolder_absolute_path = os.path.join(localdir, mass+"_reruns_snap%4.4i"%(snapshotnumber), "Rerun"+runfolder)
        Path(rerunfolder_absolute_path).mkdir(parents=True, exist_ok=True)
        print (runfolder_absolute_path)

        ##########################################
        # change working directory to rerunfolder
        ##########################################
        os.chdir(rerunfolder_absolute_path)

        ##########################################
        # copy files into rerunfolder
        ##########################################
        shutil.copy2(os.path.join(runfolder_absolute_path, "isolated_galaxy.yml"), "./")
        shutil.copy2(os.path.join(runfolder_absolute_path, "swift"), "./")
        shutil.copy2(os.path.join(runfolder_absolute_path, "output_%4.4i.hdf5"%(int(snapshotnumber))), "./")

        ##########################################
        # create ICs for rerun
        ##########################################
        ics_from_snapshot(os.path.join(rerunfolder_absolute_path, "output_%4.4i.hdf5"%(int(snapshotnumber))), \
                          os.path.join(rerunfolder_absolute_path, "ICs.hdf5"))

        ##########################################
        # update yml file for rerun
        ##########################################
        f = open("isolated_galaxy.yml", "rt")
        data = f.read()
        data = re.sub(r'^.*disk\.hdf5.*$', '  file_name:               ICs.hdf5', data, flags=re.MULTILINE)
        data = re.sub(r'^.*h_min_ratio.*$', '  h_min_ratio:           0.0001', data, flags=re.MULTILINE)
        data = re.sub(r'^.*time_end.*$', '  time_end:         0.00001025', data, flags=re.MULTILINE)
        data = re.sub(r'^.*delta_time\:  0.*$', '  delta_time:  0.00001023', data, flags=re.MULTILINE)
        data = re.sub(r'^.*dt_max\:.*$', '  dt_max:            1e-8', data, flags=re.MULTILINE)

        f.close()
        f = open("isolated_galaxy.yml", "wt")
        f.write(data)
        f.close()        

        ##########################################
        # start swift for one timestep
        ##########################################

        os.system("./swift --threads=28 --external-gravity --self-gravity --stars --star-formation --cooling --temperature --hydro --feedback --limiter --sync -n 1 isolated_galaxy.yml")










