import os
from pathlib import Path
import shutil
import glob

mass_resolution_levels = ["M5"]
softening_resolutions_in_kpc = [0.25, 0.5]
smoothing_resolutions_in_kpc = [0.0775, 0.0245, 0.00775]
star_formation_efficiencies = [0.00316]
#star_formation_efficiencies = [0.01, 0.001]

runtime_in_full_hours = 72

localdir = os.getcwd()

for mass in mass_resolution_levels:
    Path(os.path.join(localdir, mass)).mkdir(parents=True, exist_ok=True)

    for soft in softening_resolutions_in_kpc:
        for smooth in smoothing_resolutions_in_kpc:
            for sfe in star_formation_efficiencies:
                ##########################################
                # start from the original local directory
                ##########################################
                os.chdir(localdir)

                ##########################################
                # name and create runfolder
                ##########################################
                runname = "Galaxy" + mass + \
                          "_soft%4.4ipc"%(int(soft*1000)) + \
                          "_hmin%06.2fpc"%(smooth*1000.) + \
                          "_sfe%06.3f"%(sfe)

                folder=os.path.join(localdir, mass, runname) 
                Path(folder).mkdir(parents=True, exist_ok=True)

                ##########################################
                # change working directory to runfolder
                ##########################################
                os.chdir(folder)

                ##########################################
                # copy fiducidal files into runfolder
                ##########################################
                for fidfile in glob.glob("../../fiducial_files/*"):
                    shutil.copy2(fidfile, "./")   
            
                ##########################################
                # modify the yaml file for each run
                ##########################################
                f = open("isolated_galaxy.yml", "rt")
                data = f.read()
                data = data.replace('RUNNAME', runname)
                data = data.replace('SOFTENING', '%.4f'%(soft))
                data = data.replace('INITIALCONDITIONS', '%s_disk'%(mass))
                data = data.replace('HMINRATIO', '%.4f'%(smooth / (1.55 * soft)))
                data = data.replace('SFEFFICIENCY', '%.4f'%(sfe))
                data = data.replace('EOSDENSITY', '1e-4')
                data = data.replace('EOSTEMPERATURE', '10.')
                data = data.replace('EOSSLOPE', '0.')
                f.close()        

                f = open("isolated_galaxy.yml", "wt")
                f.write(data)
                f.close()        

                ##########################################
                # modify the submission script for each run
                ##########################################
                f = open("submit.job", "rt")
                data = f.read()
                data = data.replace('RUNNAME', runname)
                data = data.replace('TIMEINHOURS', '%2.2i'%(int(runtime_in_full_hours)))
                f.close()        

                f = open("submit.job", "wt")
                f.write(data)
                f.close()    

                ##########################################
                # modify the resubmission script for each run
                ##########################################
                f = open("resubmit.job", "rt")
                data = f.read()
                data = data.replace('RUNNAME', runname)
                f.close()

                f = open("resubmit.job", "wt")
                f.write(data)
                f.close()
    
                ##########################################
                # submit jobs
                ##########################################
                os.system("sbatch submit.job")




